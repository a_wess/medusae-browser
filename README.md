# Medusae-Browser

A python text-only web browser  

Simply run medusae.py to open the browser.

Uses appJar: http://appjar.info/ under Apache 2.0, see appJar/LICENSE.txt
Feel free to use the code in medusae.py for any project, attribution is not necessary.