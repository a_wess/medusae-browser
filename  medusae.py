#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 23:45:33 2019

@author: Andrew Wess 
"""

from appJar import gui
import os
import atexit
from bs4 import BeautifulSoup
import requests
import re


tab_list = [""]


def collect_webpage(request):
    """ collects the HTML file located at a web address and saves it to a text file in tmp"""
    
    webpage = BeautifulSoup(requests.get(request).content, 'html.parser')  
    filename = str(chr(len(os.listdir())+1)) + ".txt"                # creates a text file based on the number of tabs that are already open
    curr_webpage = open(filename, "w+")
    print("page information saved for " + window.getEntry("URL:"))
    for line in webpage.find_all(['title',"p", re.compile("^h[1-6]?$")]):
         
         text = line.text
         text.replace('\t','')
         curr_webpage.write(text)
         
            
    curr_webpage.close()
  
def search():
    """puts the text of a HTML file at a web address into the box (line by line)
    upon pressing the "go" button"""
    
    if (len(tab_list) <= 1):
            targ = window.getEntry("URL:")
    else:
          targ = window.getEntry("URL:" + str(window.getTabbedFrameSelectedTab("first tab")[-1]))
    if targ[0:8] != "https://" and targ[0:8] != "http://":              #adds https:// or https://www. if not already in request to get the correct webpage
        if targ[0:4] == "www.":
            targ = "https://" + targ
        else:
            targ = "https://www." + targ
            
    collect_webpage(targ)
    print("searching for: " + targ)
   # new_page = remove_html_tags().splitlines()
   
     
    origin_webpage = open( max(os.listdir()), "r")              # opens the most recent file, which will have the highest number
    new_page = origin_webpage.read()
    new_page = str(new_page).splitlines()
  
    if (len(tab_list) <= 1):
            window.updateListBox("pagecontent1",new_page)                # updates box with the text from remove_html_tags
    else:
            window.updateListBox("pagecontent" + window.getTabbedFrameSelectedTab("first tab")[-1],new_page)                # updates box with the text from remove_html_tags


    window.updateListBox("pagecontent" + window.getTabbedFrameSelectedTab("first tab")[-1],new_page)                # updates box with the text from remove_html_tags
    return targ




def close_tab(tab_name):
    """deletes the temporary text file of a tab once it has been closed"""
    
    if str(tab_name) + '.txt' in os.listdir():  
        os.remove(str(tab_name) + '.txt')




@atexit.register
def shutdown():
    """deletes all temporary files upon shutdown"""
    
    print("shuttding down...")
    if os.getcwd()[-3:] == "tmp":
        for file in os.listdir():
            os.remove(file)
        os.chdir("..")
        os.rmdir("tmp")
        



    
    
def new_tab():
    """Initializes a new tab"""
    tab_list.append("")
    window.startTab("Tab" + str(len(tab_list)))
    window.setFont(10)
    window.addLabel("title" + str(len(tab_list)), "Medusae")  #adds tab number to each widget name to prevent duplicateWidget errors
    window.setLabelBg("title" + str(len(tab_list)), "Blue")
    window.entry("URL:" + str(len(tab_list)), label=True, focus=True)
    window.setFocus("URL:" + str(len(tab_list)))
    window.buttons(["Go" + str(len(tab_list
                                   )), "New Tab" + str(len(tab_list))], [search, new_tab])
    #window.startScrollPane("scroll")
    window.addListBox("pagecontent" + str(len(tab_list)),[])
    window.stopTab()

    
    
    
if __name__ == '__main__':
    if (not(os.path.isdir("tmp"))):
        os.mkdir("tmp",0o700)
    os.chmod("tmp", 0o700)
    os.chdir("tmp")
    
    window = gui("Medusae browser", "800x400")
    window.startTabbedFrame("first tab")
    window.startTab("Tab1")  
    window.setFont(10)
    window.addLabel("title", "Medusae")  
    window.setLabelBg("title", "Blue")
    window.entry("URL:", label=True, focus=True)
    window.setFocus("URL:")
    window.buttons(["Go", "New Tab"], [search, new_tab])
    window.addListBox("pagecontent1",["Welcome to Medusae"],rowspan=2)
    print(window.getTabbedFrameSelectedTab("first tab"))
    window.stopTab()
    window.go()
    
#print(web_text + " " + str(type(web_text)) + " " + str(type("Aaa")) + " " + web_text[0:5]) 